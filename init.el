(require 'package)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

;; Add in your own as you wish:
(defvar my-packages '(starter-kit starter-kit-lisp ac-nrepl auto-complete clojure-mode erlang flymake nrepl paredit redo+ scala-mode)
  "A list of packages to ensure are installed at launch.")



(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

;; You can keep system- or user-specific customizations here
(setq dotfiles-dir (file-name-directory
                    (or (buffer-file-name) load-file-name)))
(setq system-specific-config (concat dotfiles-dir system-name ".el")
      user-specific-config (concat dotfiles-dir user-login-name ".el")
      user-specific-dir (concat dotfiles-dir "user"))
(add-to-list 'load-path user-specific-dir)

(if (file-exists-p system-specific-config) (load system-specific-config))
(if (file-exists-p user-specific-config) (load user-specific-config))
(if (file-exists-p user-specific-dir)
  (mapc #'load (directory-files user-specific-dir nil ".*el$")))

;;; init.el ends here
