(require 'flymake)

;; duplicate line
(defun duplicate-line-or-region (&optional n)
  "Duplicate current line, or region if active.
With argument N, make N copies.
With negative N, comment out original line and use the absolute value."
  (interactive "*p")
  (let ((use-region (use-region-p)))
    (save-excursion
      (let ((text (if use-region        ;Get region if active, otherwise line
                      (buffer-substring (region-beginning) (region-end))
                    (prog1 (thing-at-point 'line)
                      (end-of-line)
                      (if (< 0 (forward-line 1)) ;Go to beginning of next line, or make a new one
                          (newline))))))
        (dotimes (i (abs (or n 1)))     ;Insert N times, or once if not specified
          (insert text))))
    (if use-region nil                  ;Only if we're working with a line (not a region)
      (let ((pos (- (point) (line-beginning-position)))) ;Save column
        (if (> 0 n)                             ;Comment out original with negative arg
            (comment-region (line-beginning-position) (line-end-position)))
        (forward-line 1)
        (forward-char pos)))))

(global-set-key (kbd "M-f") 'duplicate-line-or-region)

;; buffer cycling
(defun switch-to-previous-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer)))
(global-set-key (kbd "<backtab>") 'switch-to-previous-buffer)


;; try to get clipboard working
; stops selection with a mouse being immediately injected to the kill ring
(setq mouse-drag-copy-region nil)
; stops killing/yanking interacting with primary X11 selection
(setq x-select-enable-primary nil)
; makes killing/yanking interact with clipboard X11 selection
(setq x-select-enable-clipboard t)

; active region sets primary X11 selection
(setq select-active-regions t)
; make mouse middle-click only paste from primary X11 selection, not
; clipboard and kill ring.
(global-set-key [mouse-2] 'mouse-yank-primary)
;;

;; vi-like match parens
(defun match-parenthesis (arg)
  "Match the current character according to the syntax table.

   Based on the freely available match-paren.el by Kayvan Sylvan.
   I merged code from goto-matching-paren-or-insert and match-it.

   You can define new \"parentheses\" (matching pairs).
   Example: angle brackets. Add the following to your .emacs file:

   	(modify-syntax-entry ?< \"(>\" )
   	(modify-syntax-entry ?> \")<\" )

   Simon Hawkin <cema@cs.umd.edu> 03/14/1998"
  (interactive "p")
  (let
      ((syntax (char-syntax (following-char))))
    (cond
     ((= syntax ?\()
      (forward-sexp 1) (backward-char))
     ((= syntax ?\))
      (forward-char) (backward-sexp 1))
     (t (message "No match"))
     )
    ))
(global-set-key (kbd "<f6>") 'match-parenthesis)

;; window size
(defun set-frame-size-according-to-resolution ()
  (interactive)
  (if window-system
  (progn
    ;; use 120 char wide window for largeish displays
    ;; and smaller 80 column windows for smaller displays
    ;; pick whatever numbers make sense for you
    (if (> (x-display-pixel-width) 1280)
        (add-to-list 'default-frame-alist (cons 'width 180))
      (add-to-list 'default-frame-alist (cons 'width 80)))
    ;; for the height, subtract a couple hundred pixels
    ;; from the screen height (for panels, menubars and
    ;; whatnot), then divide by the height of a char to
    ;; get the height we want
    (add-to-list 'default-frame-alist 
                 (cons 'height (/ (- (x-display-pixel-height) 50) (frame-char-height)))))))

(set-frame-size-according-to-resolution)


; lock at home autosaves and backups
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

; плавный скроллинг
(setq scroll-step 1
      scroll-conservatively 100000; не прыгать на середину страницы при скроллинге
;      scroll-margin 7; начинать промотку страницы за 5 сток до края
      scroll-preserve-screen-position t
      auto-window-vscroll nil)

; charsets
(set-coding-system-priority 'utf-8 'cp1251 'emacs-mule)

; search goodies
(defun my-isearch-word-at-point ()
  (interactive)
  (call-interactively 'isearch-forward-regexp))

(defun my-isearch-yank-word-hook ()
  (when (equal this-command 'my-isearch-word-at-point)
    (let ((string (concat "\\<"
                          (buffer-substring-no-properties
                            (progn (skip-syntax-backward "w_") (point))
                            (progn (skip-syntax-forward "w_") (point)))
                          "\\>")))
      (if (and isearch-case-fold-search
               (eq 'not-yanks search-upper-case))
        (setq string (downcase string)))
      (setq isearch-string string
            isearch-message
            (concat isearch-message
                    (mapconcat 'isearch-text-char-description
                               string ""))
            isearch-yank-flag t)
      (isearch-search-and-update))))

(add-hook 'isearch-mode-hook 'my-isearch-yank-word-hook)
(global-set-key (kbd "<f8>") 'my-isearch-word-at-point)


;; cua and friends
(cua-mode t)
(transient-mark-mode t) ; highlight text selection
(delete-selection-mode t) ; delete seleted text when typing

; show line number the cursor is on, in status bar (the mode line)
(line-number-mode t)
(column-number-mode t)

;;
(defun my-expand-file-name-at-point ()
  "Use hippie-expand to expand the filename"
  (interactive)
  (let ((hippie-expand-try-functions-list '(try-complete-file-name-partially try-complete-file-name)))
    (call-interactively 'hippie-expand)))
(global-set-key (kbd "C-c /") 'my-expand-file-name-at-point)


;; (setq cua-keep-region-after-copy t)
;; (if (fboundp 'pc-selection-mode)
;;     (pc-selection-mode)
;;         (require 'pc-select))
;; (delete-selection-mode 1)

;; tree undo
;(require 'undo-tree)
;(global-undo-tree-mode t)
;(eval-after-load "undo-tree"
  ;`(progn
     ;(global-set-key (kbd "C-z") 'undo-tree-undo)
     ;(global-set-key (kbd "C-t") 'undo-tree-redo)))

;; redo
(require 'redo+)
(global-set-key (kbd "C-t") 'redo)


;; nxHTML
;(load "nxhtml/autostart.el")
;(setq mumamo-chunk-coloring 'submode-colored
      ;nxhtml-skip-welcome t
      ;indent-region-mode t
      ;rng-nxml-auto-validate-flag nil)

;; TT mode no good, so use nxhtml for TTs
;(setq auto-mode-alist
    ;(append '(("\\.tt$" . nxhtml-mode))  auto-mode-alist ))


;; autocomplete
;(require 'popup)
;(add-to-list 'load-path "~/.emacs.d")    ; This may not be appeared if you have already added.
(require 'auto-complete-config)
(ac-config-default)
;(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
;;; nrepl
;(require 'ac-nrepl)
(add-hook 'nrepl-mode-hook 'ac-nrepl-setup)
(add-hook 'nrepl-interaction-mode-hook 'ac-nrepl-setup)
(eval-after-load "auto-complete"
                 '(add-to-list 'ac-modes 'nrepl-mode))
;;(add-to-list 'ac-sources 'ac-source-slime-simple)
;;(add-hook 'slime-mode-hook 'set-up-slime-ac)
;;(add-hook 'slime-repl-mode-hook 'set-up-slime-ac)
;;(add-hook 'slime-repl-mode-hook 'clojure-mode-font-lock-setup)

;; hook AC into completion-at-point
(defun set-auto-complete-as-completion-at-point-function ()
   (setq completion-at-point-functions '(auto-complete)))

(add-hook 'auto-complete-mode-hook 'set-auto-complete-as-completion-at-point-function)
(add-hook 'nrepl-mode-hook 'set-auto-complete-as-completion-at-point-function)
(add-hook 'nrepl-interaction-mode-hook 'set-auto-complete-as-completion-at-point-function)

;;; nrepl
(defun save-and-eval ()
    (interactive)
    (save-some-buffers 1)
;    (nrepl-set-ns (nrepl-current-ns))
    (nrepl-load-current-buffer))

(defun my-nrepl-hook ()
  ;;(flymake-mode 1)
  (setq nrepl-popup-stacktraces nil)
  ;; customize keys
  (global-set-key (kbd "M-e") 'nrepl-eval-expression-at-point)
  (local-set-key [f5] 'save-and-eval))
(add-hook 'nrepl-interaction-mode-hook 'my-nrepl-hook)
;(setq nrepl-popup-stacktraces nil)

;; slime
;; slime
;;(setq inferior-lisp-program "/usr/bin/sbcl")
;;
;;(add-to-list 'load-path "~/.emacs.d/slime")
;(require 'slime)
;(slime-setup)

;(eval-after-load "slime"
  ;`(progn
;;;     (slime-setup '(slime-repl))
     ;(global-set-key (kbd "M-e") 'slime-eval-buffer)
     ;(global-set-key (kbd "C-c C-.") 'slime-quit-lisp)
     ;(custom-set-variables
      ;'(inhibit-splash-screen t)
      ;'(slime-complete-symbol*-fancy t)
      ;'(slime-net-coding-system 'utf-8-unix)
      ;'(slime-startup-animation nil)
      ;'(swank-clojure-class-path "/opt/clojure-1.3.0/clojure.jar")
      ;'(swank-clojure-extra-classpaths (list
                                       ;"/opt/clojure-1.3.0/clojure-contrib.jar"))
      ;'(slime-lisp-implementations '((sbcl ("/usr/bin/sbcl")))))))

(defun lein-swank ()
  (interactive)
  (let ((slime-port 4005)
        (root (locate-dominating-file default-directory "project.clj")))
    (when (not root)
      (error "Not in a Leiningen project."))
    ;; you can customize slime-port using .dir-locals.el
    (shell-command (format "cd %s && lein swank %s &" root slime-port)
                   "*lein-swank*")
    (set-process-filter (get-buffer-process "*lein-swank*")
                        (lambda (process output)
                          (when (string-match "Connection opened on" output)
                            (slime-connect "localhost" slime-port)
                            (set-process-filter process nil))))
    (message "Starting swank server...")))


;; compile and save
(defun save-all-and-compile ()
    (interactive)
    (save-some-buffers 1)
    (erlang-compile))

;; erlang
(defun flymake-erlang-init ()
  (let* ((temp-file (flymake-init-create-temp-buffer-copy
                      'flymake-create-temp-inplace))
         (local-file (file-relative-name temp-file
                                         (file-name-directory buffer-file-name))))
    (list "~/.emacs.d/flymake_erlang.sh" (list local-file))))
(add-to-list 'flymake-allowed-file-name-masks '("\\.erl\\'" flymake-erlang-init))

(defun my-erlang-mode-hook ()
  (flymake-mode 1)
  (setq erlang-root-dir "/usr/lib/erlang")
  (setq erlang-compile-extra-opts '(debug_info (i . \"../include\")))
  ;; when starting an Erlang shell in Emacs, default in the node name
  ;;(setq inferior-erlang-machine-options (list "-sname" "emacs" "-mnesia" "dir" "\"mnesia\""))
  (setq inferior-erlang-machine-options (list "+A" "10" "-sname" "emacs" "-mnesia" "dir" "\"/tmp\""))
  ;; customize keys
  (local-set-key [return] 'newline-and-indent)
  (local-set-key [f5] 'save-all-and-compile))

(add-hook 'erlang-mode-hook 'my-erlang-mode-hook)

;; distel
;(add-to-list 'load-path "~/.emacs.d/distel/elisp")
;(require 'distel)
;(distel-setup)


;; Haskell
(add-to-list 'load-path "~/.emacs.d/haskellmode-emacs/")
;;(load "~/.emacs.d/haskellmode-emacs/haskell-site-file")
;;(add-to-list 'load-path "~/.cabal/share/scion-0.2.0.2/emacs")
;;(require 'scion)

(add-hook 'haskell-mode-hook 'my-haskell-mode-hook)
(defun my-haskell-mode-hook ()
  (setq haskell-saved-check-command (concat "ghc -fbyte-code -v -i" (file-name-directory buffer-file-name)))
  ;(setq haskell-saved-check-command "hlint")
  (flymake-mode 1)
;;  (scion-mode 1)
;;  (scion-flycheck-on-save 1)
  (set (make-local-variable 'multiline-flymake-mode) t)
  (turn-on-haskell-doc-mode)
  (turn-on-haskell-indentation)
  (local-set-key [f5] 'inferior-haskell-load-file))

(defun flymake-Haskell-init ()
  (flymake-simple-make-init-impl
   'flymake-create-temp-with-folder-structure nil nil
   (file-name-nondirectory buffer-file-name)
   'flymake-get-Haskell-cmdline))
;(defun flymake-Haskell-init () 
  ;(let* ((temp-file       (flymake-init-create-temp-buffer-copy 
                           ;'flymake-create-temp-inplace)) 
         ;(local-file  (file-relative-name 
                       ;temp-file 
                       ;(file-name-directory buffer-file-name)))) 
    ;(list "ghc" (list "--make" "-fno-code" local-file "-i")))) 


(defun flymake-get-Haskell-cmdline (source base-dir)
  (list "ghc"
        (list "--make" "-fbyte-code"
              (concat "-i"base-dir)  ;;; can be expanded for additional -i options as in the Perl script
              source)))

(defvar multiline-flymake-mode nil)
(defvar flymake-split-output-multiline nil)

;; this needs to be advised as flymake-split-string is used in other places and I don't know of a better way to get at the caller's details
(defadvice flymake-split-output
  (around flymake-split-output-multiline activate protect)
  (if multiline-flymake-mode
      (let ((flymake-split-output-multiline t))
        ad-do-it)
    ad-do-it))

(defadvice flymake-split-string
  (before flymake-split-string-multiline activate)
  (when flymake-split-output-multiline
    (ad-set-arg 1 "^\\s *$")))

;; Why did nobody tell me about eval-after-load - very useful
(eval-after-load "flymake"
  '(progn
     (add-to-list 'flymake-allowed-file-name-masks
                  '("\\.l?hs$" flymake-Haskell-init flymake-simple-java-cleanup))
     (add-to-list 'flymake-err-line-patterns
                  '("^\\(.+\\.l?hs\\):\\([0-9]+\\):\\([0-9]+\\):\\(\\(?:.\\|\\W\\)+\\)"
                    1 2 3 4))))

;; ocaml
(defun my-tuareg-mode-hook ()
  ;; when starting an Erlang shell in Emacs, default in the node name
  (local-set-key (kbd "M-e") 'tuareg-eval-buffer))

(add-hook 'tuareg-mode-hook 'my-tuareg-mode-hook)

;; Scala
;; load the ensime lisp code...
(add-to-list 'load-path "/opt/ensime_2.9.2-0.9.8.1/elisp/")
(require 'ensime)
;;
;; ;; This step causes the ensime-mode to be started whenever
;; ;; scala-mode is started for a buffer. You may have to customize this step
;; ;; if you're not using the standard scala mode.
(add-hook 'scala-mode-hook 'ensime-scala-mode-hook)

