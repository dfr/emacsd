
;; CTRL-W
(defun backward-kill-word-or-kill-region (arg)
  (interactive "p")
  (if (region-active-p)
    (kill-region (region-beginning) 
                 (region-end))
    (backward-kill-word arg)))


(global-set-key (kbd "C-w") 'backward-kill-word-or-kill-region)
(define-key minibuffer-local-map (kbd "C-w") 'backward-kill-word-or-kill-region)
(add-hook 'ido-setup-hook 
          (lambda ()
            (define-key ido-completion-map (kbd "C-w") 'ido-delete-backward-word-updir)))


;; misc keys
(global-set-key (kbd "C-y") 'kill-whole-line)
(global-set-key (kbd "C-a") 'mark-whole-buffer)
(global-set-key (kbd "C-SPC") 'mark-sexp)
(global-set-key (kbd "C-M-/") 'comment-or-uncomment-region)
;;(global-set-key (kbd "C-M-z") 'kill-emacs)
;;(global-set-key (kbd "C-x C-z") 'kill-emacs)
(global-set-key (kbd "C-x C-k") 'kill-this-buffer)
(global-set-key (kbd "<f5>") 'sr-speedbar-toggle)
(global-set-key (kbd "<f2>") 'save-buffer)
(global-set-key (kbd "M-%") 'replace-regexp)
(global-set-key (kbd "M-i") 'imenu)
(global-set-key (kbd "C-x TAB") 'indent-rigidly)

(global-set-key (kbd "<C-tab>") (lambda () (interactive) (other-window -1)))
(global-set-key (kbd "<C-S-tab>") (lambda () (interactive) (other-window 1)))

;;(global-set-key (kbd "TAB") 'self-insert-command)
(global-set-key (kbd "RET") 'newline-and-indent)

(global-set-key (kbd "C-c /") 'ac-complete-filename)
